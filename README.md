# Capital One Hackathon Sandbox

Sandbox of scripts and other things in preparation for the Capital One Hackathon on 11/4-11/5.

Includes:
- Likely schema definition, implemented in the database and exposed through REST endpoints

### Instructions

To generate the schema:

```bash
chmod +x generate_schema.sh
./generate_schema.sh
```
